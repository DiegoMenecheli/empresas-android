package com.example.empresas.repository

import com.example.empresas.network.AuthApi
import com.example.empresas.preferences.UserPreferences


class  AuthRepository(
    private val api : AuthApi,
    private val preferences: UserPreferences
):BaseRepository(){
    suspend fun login(
        email : String ,
        password: String
    )= safeApiCall { api.login(email,password) }


    suspend fun saveAuthToken(token:String)
    {
        preferences.saveAuthToken(token)
    }

    suspend fun saveAuthClient(client:String)
    {
        preferences.saveAuthClient(client)
    }

    suspend fun saveAuthUid(uid: Int)
    {
        preferences.saveAuthUid(uid)
    }
}