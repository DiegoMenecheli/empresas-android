package com.example.empresas.repository

import com.example.empresas.network.UserApi

class UserRepository(
    private val api : UserApi
) : BaseRepository(){
    suspend fun getUser() = safeApiCall {
        api.getUser()
    }

}