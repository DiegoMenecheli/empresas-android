package com.example.empresas.network

import com.example.empresas.responses.userdetails.UserResponse
import retrofit2.http.GET

interface UserApi {

   @GET("user")
   suspend fun getUser(): UserResponse

}