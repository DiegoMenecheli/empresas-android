package com.example.empresas.network

import com.example.empresas.BASE_URL
import com.example.empresas.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RemoteDataSource {

    fun<Api> buildApi(
        api: Class<Api>,
        authToken:String?= null,
        authClient:String?= null,
        authUid: Int? = null
    ): Api{
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(
                OkHttpClient.Builder()
                    .addInterceptor {
                        chain -> chain.proceed(chain.request().newBuilder().also {
                        it.addHeader("access-token" ,"$authToken")
                        it.addHeader("client", "$authClient")
                        it.addHeader("uid", "$authUid")
                    }.build())
                    }
                    .also { client ->
                    if(BuildConfig.DEBUG){
                        val logging = HttpLoggingInterceptor()
                        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
                        client.addInterceptor(logging)
                    }
                }.build()
            )
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(api)
    }
}