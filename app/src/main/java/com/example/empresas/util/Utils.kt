package com.example.empresas.util

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import com.example.empresas.network.Resource
import com.example.empresas.ui.auth.LoginFragment
import com.google.android.material.snackbar.Snackbar

fun<A : Activity> Activity.startNewActivity(activity: Class<A>){
    Intent(this, activity).also {
        it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(it)
    }
}


fun View.enable(enabled: Boolean){
    isEnabled = enabled
    alpha = if(enabled) 1f else 0.5f
}


fun View.snackbar(message:String ,action:(()-> Unit)? = null) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_LONG)
    action?.let {
        snackbar.setAction("Tente novamente") {
            it()
        }
    }
    snackbar.show()

}

    fun Fragment.handleApiError(
        failure: Resource.Failure,
        retry : (()->Unit)? = null
    ){
        when{
            failure.isNetworkError -> requireView().snackbar(
                "Sem conexão com a internet",
                retry
            )
            failure.errorCode == 401 -> {
                if(this is LoginFragment)
                {
                    requireView().snackbar("Email ou senha inválidos")
                }
            }

            else ->{
                val error = failure.errorBody?.string().toString()
                requireView().snackbar(error)
            }
        }
    }
