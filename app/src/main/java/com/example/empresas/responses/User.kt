package com.example.empresas.responses

data class User(
    val access_token: String,
    val uid: Int,
    val client: String
)