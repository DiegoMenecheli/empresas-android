package com.example.empresas.responses.userdetails

data class User(
    val password: String,
    val email: String
)