package com.example.empresas.responses.userdetails

data class UserResponse(
    val user: User
)