package com.example.empresas.responses

data class LoginResponse(
    val user: User
)