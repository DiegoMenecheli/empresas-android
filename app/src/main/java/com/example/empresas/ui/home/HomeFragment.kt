package com.example.empresas.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.example.empresas.databinding.FragmentHomeBinding
import com.example.empresas.network.UserApi
import com.example.empresas.repository.UserRepository
import com.example.empresas.ui.base.BaseFragment
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking

class HomeFragment : BaseFragment<HomeViewModel, FragmentHomeBinding, UserRepository>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getUser()
        viewModel.user.observe(viewLifecycleOwner, Observer {

        })

    }

    override fun getViewModel() = HomeViewModel::class.java

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentHomeBinding.inflate(inflater, container, false)

    override fun getFragmentRepository(): UserRepository {
        val token = runBlocking { userPreferences.authToken.first() }
        val client = runBlocking { userPreferences.authClient.first() }
        val uid = runBlocking { userPreferences.authUid.first() }
        val api = remoteDataSource.buildApi(UserApi::class.java, token, client, uid)
        return UserRepository(api)

    }


}