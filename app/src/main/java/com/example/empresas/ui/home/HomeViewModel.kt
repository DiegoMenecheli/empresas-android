package com.example.empresas.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.empresas.network.Resource
import com.example.empresas.repository.UserRepository
import com.example.empresas.responses.userdetails.UserResponse
import com.example.empresas.ui.base.BaseViewModel
import kotlinx.coroutines.launch

class HomeViewModel(private val repository : UserRepository): BaseViewModel(repository) {

    private  val _user : MutableLiveData<Resource<UserResponse>> = MutableLiveData()
    val user : LiveData<Resource<UserResponse>>
    get() = _user

    fun getUser() = viewModelScope.launch {
        _user.value = Resource.loading
        _user.value = repository.getUser()
    }
}