package com.example.empresas.ui.base

import androidx.lifecycle.ViewModel
import com.example.empresas.repository.BaseRepository

abstract class BaseViewModel(
    private val repository: BaseRepository
): ViewModel(){
}