package com.example.empresas.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.viewbinding.ViewBinding
import com.example.empresas.network.RemoteDataSource
import com.example.empresas.network.UserApi
import com.example.empresas.preferences.UserPreferences
import com.example.empresas.repository.BaseRepository
import com.example.empresas.ui.auth.AuthActivity
import com.example.empresas.util.startNewActivity
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch


abstract class BaseFragment<VM: BaseViewModel ,B: ViewBinding,R: BaseRepository> : Fragment() {

    protected lateinit var binding : B
    protected  val remoteDataSource = RemoteDataSource()
    protected lateinit var viewModel: VM
    protected lateinit var userPreferences: UserPreferences
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
            userPreferences = UserPreferences(requireContext())
            binding = getFragmentBinding(inflater,container)
            val factory = ViewModelFactory(getFragmentRepository())
            viewModel = ViewModelProvider(this, factory).get(getViewModel())
            return binding.root
    }

    abstract fun getViewModel() : Class<VM>
    abstract fun getFragmentBinding(inflater: LayoutInflater,container: ViewGroup?): B
    abstract fun getFragmentRepository(): R
}
