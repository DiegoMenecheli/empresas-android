package com.example.empresas.ui.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.empresas.network.Resource
import com.example.empresas.repository.AuthRepository
import com.example.empresas.responses.LoginResponse
import com.example.empresas.ui.base.BaseViewModel
import kotlinx.coroutines.launch

class AuthViewModel(
    private  val repository: AuthRepository
) : BaseViewModel(repository) {
    private val _loginResponse: MutableLiveData<Resource<LoginResponse>> = MutableLiveData()
    val loginResponse: LiveData<Resource<LoginResponse>>
        get() = _loginResponse

    fun login(
        email: String,
        password: String
    ) = viewModelScope.launch {
        _loginResponse.value = Resource.loading
        _loginResponse.value = repository.login(email, password)
    }

   suspend fun saveAuthToken(token: String)  {
        repository.saveAuthToken(token)
    }

    suspend fun saveAuthClient(client: String)  {
        repository.saveAuthClient(client)
    }

    suspend fun saveAuthUid(uid: Int)  {
        repository.saveAuthUid(uid)
    }

}