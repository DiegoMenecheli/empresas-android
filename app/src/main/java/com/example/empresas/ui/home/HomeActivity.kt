package com.example.empresas.ui.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.empresas.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}