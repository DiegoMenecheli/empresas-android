package com.example.empresas.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.example.empresas.databinding.FragmentLoginBinding
import com.example.empresas.network.AuthApi
import com.example.empresas.network.Resource
import com.example.empresas.repository.AuthRepository
import com.example.empresas.ui.base.BaseFragment
import com.example.empresas.ui.home.HomeActivity
import com.example.empresas.util.enable
import com.example.empresas.util.handleApiError
import com.example.empresas.util.startNewActivity

import kotlinx.coroutines.launch

class LoginFragment : BaseFragment<AuthViewModel, FragmentLoginBinding, AuthRepository>() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.loginResponse.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Resource.Success -> {
                    lifecycleScope.launch {
                        viewModel.saveAuthToken(it.value.user.access_token)
                        viewModel.saveAuthClient(it.value.user.client)
                        viewModel.saveAuthUid(it.value.user.uid)
                        requireActivity().startNewActivity(HomeActivity::class.java)
                    }
                }
                is Resource.Failure -> handleApiError(it){login()}
            }
        })


        binding.password.addTextChangedListener {
            val email = binding.username.text.toString().trim()
            binding.btnSubmit.enable(email.isNotEmpty() && it.toString().isNotEmpty())
        }


        binding.btnSubmit.setOnClickListener {
            login()
        }


    }

    private fun login(){
        val email = binding.username.text.toString().trim()
        val password = binding.password.text.toString().trim()
        viewModel.login(email, password)
    }
    override fun getViewModel() = AuthViewModel::class.java

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentLoginBinding.inflate(inflater, container, false)

    override fun getFragmentRepository() =
        AuthRepository(remoteDataSource.buildApi(AuthApi::class.java),userPreferences)

}