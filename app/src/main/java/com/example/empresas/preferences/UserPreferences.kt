package com.example.empresas.preferences

import android.content.Context
import androidx.datastore.DataStore
import androidx.datastore.preferences.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class UserPreferences(
    context: Context

){
    private val applicationContext = context.applicationContext
    private val dataStore: DataStore<Preferences>

    init {
        dataStore = applicationContext.createDataStore(
            name = "my_data_store"
        )
    }

    val authToken: Flow<String?>
        get() = dataStore.data.map { preferences ->
            preferences[KEY_AUTH]
        }

    suspend fun saveAuthToken(authToken: String) {
        dataStore.edit { preferences ->
            preferences[KEY_AUTH] = authToken
        }
    }

    val authClient: Flow<String?>
        get() = dataStore.data.map { preferences ->
            preferences[KEY_CLIENT]
        }

    suspend fun saveAuthClient(authClient: String) {
        dataStore.edit { preferences ->
            preferences[KEY_CLIENT] = authClient
        }
    }

    val authUid: Flow<Int?>
        get() = dataStore.data.map { preferences ->
            preferences[KEY_UDI]
        }

    suspend fun saveAuthUid(authUid: Int) {
        dataStore.edit { preferences ->
            preferences[KEY_UDI] = authUid
        }
    }

    suspend fun clear(){
        dataStore.edit {
            preferences -> preferences.clear()
        }
    }


    companion object {
        private val KEY_AUTH = preferencesKey<String>("access-token")
        private val KEY_CLIENT = preferencesKey<String>("client")
        private val KEY_UDI = preferencesKey<Int>("uid")
    }
}